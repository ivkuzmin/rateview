package com.jaka.rateview.ui

import android.support.v7.app.AppCompatActivity
import com.yandex.metrica.YandexMetrica

open class RateviewBaseActivity : AppCompatActivity(){

    
    override fun onResume() {
        super.onResume()
        YandexMetrica.onResumeActivity(this)
    }

    override fun onPause() {
        super.onPause()
        YandexMetrica.onPauseActivity(this)
    }
}