package com.jaka.rateview.ui.dashboard

import com.jaka.rateview_remote.service.repository.RateRepository
import io.reactivex.android.schedulers.AndroidSchedulers

class DashboardPresenter(private val dashboardView: DashboardContract.DashboardView) : DashboardContract.DashboardPresener {

    init {
        dashboardView.setPresenter(this)
    }

    override fun subscribe() {
        loadDashboard()
    }

    fun loadDashboard() {
        RateRepository.getInstance().getTicker()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { result -> dashboardView.showDashboard(result)},
                        { error -> dashboardView.showError(error.localizedMessage) })

    }

}