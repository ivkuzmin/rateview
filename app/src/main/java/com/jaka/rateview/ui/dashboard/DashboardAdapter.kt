package com.jaka.rateview.ui.dashboard

import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.jaka.rateview.R
import com.jaka.rateview_model.domain.Rate
import com.jaka.rateview_model.domain.Ticker
import kotlinx.android.synthetic.main.v_rate.view.*

/**
 * Created by Илья on 16.03.2018.
 */
class DashboardAdapter() : RecyclerView.Adapter<DashboardAdapter.RateViewHolder>() {

    var rates: ArrayList<Ticker>? = null
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RateViewHolder {
        return RateViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.v_rate, parent,false))
    }

    override fun getItemCount(): Int {
        return rates?.size ?: 0
    }

    override fun onBindViewHolder(holder: RateViewHolder, position: Int) {
        holder.bind(rates?.get(position)?.ticker)
    }

    class RateViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val rateName: TextView = itemView.findViewById(R.id.rate_name)
        private val ratePrice: TextView = itemView.findViewById(R.id.rate_price)
        private val rateTarget: TextView = itemView.findViewById(R.id.rate_target)

        fun bind(rate: Rate?) {
            rateName.text = rate?.base
            ratePrice.text = rate?.price
            rateTarget.text = rate?.target
        }


    }
}