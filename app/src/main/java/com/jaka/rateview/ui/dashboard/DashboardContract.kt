package com.jaka.rateview.ui.dashboard

import com.jaka.rateview.mvp.BasePresenter
import com.jaka.rateview.mvp.BaseView
import com.jaka.rateview_model.domain.Ticker

interface DashboardContract {
    interface DashboardPresener : BasePresenter

    interface DashboardView : BaseView<DashboardPresener> {
        fun showDashboard(rate: ArrayList<Ticker>?)
        fun showError(reason: String)
    }
}