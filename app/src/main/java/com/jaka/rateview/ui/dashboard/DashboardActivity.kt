package com.jaka.rateview.ui.dashboard

import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.widget.Toast
import com.jaka.rateview.R
import com.jaka.rateview.ui.RateviewBaseActivity
import com.jaka.rateview_model.domain.Ticker

class DashboardActivity : RateviewBaseActivity(), DashboardContract.DashboardView {

    var dashboardListView: RecyclerView? = null
    var dashboardAdapter: DashboardAdapter = DashboardAdapter()

    override fun showError(reason: String) {
        Toast.makeText(this, reason, Toast.LENGTH_LONG).show()
        Log.e("RV", reason)
    }

    override fun showDashboard(rate: ArrayList<Ticker>?) {
        Log.d("RV", rate?.toString())
        dashboardAdapter.rates = rate

    }

    var dashboardPresenter: DashboardContract.DashboardPresener? = null

    override fun setPresenter(presenter: DashboardContract.DashboardPresener) {
        dashboardPresenter = presenter
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.a_dashboard)
        dashboardListView = findViewById(R.id.dashboard_list_view)
        dashboardPresenter = DashboardPresenter(this)
        dashboardListView?.adapter = dashboardAdapter
    }

    override fun onResume() {
        super.onResume()
        dashboardPresenter?.subscribe()
    }
}