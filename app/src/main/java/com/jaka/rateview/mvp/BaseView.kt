package com.jaka.rateview.mvp

interface BaseView<in P> {

    fun setPresenter(presenter: P)

}