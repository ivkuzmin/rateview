package com.jaka.rateview

import android.app.Application
import com.yandex.metrica.YandexMetrica

/**
 * Created by Илья on 01.02.2018.
 */
class RateviewApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        YandexMetrica.activate(this,BuildConfig.APP_METRICA_API_KEY)
        YandexMetrica.enableActivityAutoTracking(this)
    }
}