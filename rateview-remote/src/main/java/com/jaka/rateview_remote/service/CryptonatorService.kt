package com.jaka.rateview_remote.service

import com.jaka.rateview_model.domain.Ticker
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path

/**
 * Created by Илья on 15.03.2018.
 */
interface CryptonatorService {
    @GET("full/{baseToTarget}")
    fun getTicker(@Path("baseToTarget") baseToTarget: String): Observable<Ticker>
}