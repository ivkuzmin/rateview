package com.jaka.rateview_remote.service.repository

import com.jaka.rateview_model.domain.Ticker
import com.jaka.rateview_remote.service.ServiceManager
import io.reactivex.Observable

/**
 * Created by Илья on 15.03.2018.
 */
class RateRepository {

    companion object {
        fun getInstance(): RateRepository {
            return RateRepository()
        }
    }

    fun getTicker() : Observable<ArrayList<Ticker>> {
        return ServiceManager().getRates()
    }
}