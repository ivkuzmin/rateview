package com.jaka.rateview_remote.service


import com.jaka.rateview_model.domain.Ticker
import io.reactivex.Observable
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by Илья on 15.03.2018.
 */
class ServiceManager {

    enum class Coins(var baseToTarget: String) {
        ETH("eth-usd"), BTC("btc-usd")
    }

    private companion object Cryptonator {
        fun initRetorfit(): CryptonatorService {
            val retrofit: Retrofit = Retrofit.Builder().baseUrl("https://api.cryptonator.com/api/")
                    .addCallAdapterFactory(
                            RxJava2CallAdapterFactory.create())
                    .addConverterFactory(
                            GsonConverterFactory.create())
                    .build()
            return retrofit.create(CryptonatorService::class.java)
        }
    }

    val cryptonatorApi by lazy {
        Cryptonator.initRetorfit()
    }

    fun getRates(): Observable<ArrayList<Ticker>> {
        var eth: Observable<Ticker> = cryptonatorApi.getTicker(Coins.ETH.baseToTarget)
        var btc: Observable<Ticker> = cryptonatorApi.getTicker(Coins.BTC.baseToTarget)
        return Observable.zip(eth, btc, BiFunction<Ticker,Ticker,ArrayList<Ticker>>
        {t1,t2->
            run {
                val tickers:ArrayList<Ticker> = ArrayList<Ticker>()
                tickers.add(t1)
                tickers.add(t2)
                tickers
            }
        })
                .subscribeOn(Schedulers.io())
    }
}